;;; rcscope.el --- Cscope interface for GNU Emacs 25 and up -*- lexical-binding: t; -*-

;; Copyright (C) 2014-2016 Robert Jarzmik <robert.jarzmik@free.fr>
;; Copyright (C) 2018, 2023 Vladimir Sedach <vas@oneofus.la>

;; SPDX-License-Identifier: GPL-3.0-or-later

;; Authors: Robert Jarzmik <robert.jarzmik@free.fr>
;;          Vladimir Sedach <vas@oneofus.la>

;; Maintainer: Vladimir Sedach <vas@oneofus.la>

;; Keywords: tools
;; URL: https://savannah.nongnu.org/projects/rcscope

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see
;; <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Cscope (http://cscope.sourceforge.net/) is a powerful and efficient
;; code browsing and cross referencing tool for the C programming
;; language.

;; RCscope is Cscope interface for the GNU Emacs 25 era.

;; Using Cscope, you can easily search for where symbols are used and
;; defined. Cscope is designed to answer questions like:
;;
;; Where is this variable used?
;; What is the value of this preprocessor symbol?
;; Where is this function in the source files?
;; What functions call this function?
;; What functions are called by this function?
;; Where does the message "out of space" come from?
;; Where is this source file in the directory structure?
;; What files include this header file?

;; RCscope is written to follow GNU Emacs `compilation-mode' and
;; `occur-mode' conventions. Query results can be navigated using
;; `next-error'/`previous-error', just like `grep' and `occur'
;; searches.

;; RCscope automatically manages Cscope index files and index updates
;; (using projectile.el and the -R Cscope option), and uses persistent
;; Cscope processes that also support working on remote projects with
;; TRAMP.

;; In addition to standard Cscope queries, RCscope also provides a
;; tree visualization of a function's callers in a format similar to
;; Steve Baker's Tree (http://mama.indstate.edu/users/ice/tree/)
;; directory listing utility.

;; The recommended configuration for RCscope is as a hook to `c-mode':

;; (use-package rcscope
;;   :hook (c-mode . rcscope-mode))

;; By default, RCscope query commands live under the "M-s c" prefix

;; RCscope is based on Robert Jarzmik's rscope:
;; https://github.com/rjarzmik/rscope

(require 'cl)
(require 'expand-region)
(require 'projectile)

(defgroup rcscope nil
  "The RCscope interface to the Cscope code browsing tool for C."
  :group 'tools
  :prefix "rcscope-"
  :link '(emacs-commentary-link :tag "Commentary" "rcscope"))

(defcustom rcscope-keymap-prefix (kbd "M-s c")
  "Prefix for rcscope commands.
Binds under default `search-map' binding by default."
  :type 'string
  :group 'rcscope)

(defcustom rcscope-cscope-options '("-R" "-q" "-k")
  "Default options to pass to Cscope.
See the cscope(1) man page for documentation.
The default include dir setting will not return accurate results
on some systems and so is disabled by default (-k)"
  :type 'list
  :group 'rcscope)

(defvar rcscope-mode-menu-map
  (let ((map (make-sparse-keymap)))
    (bindings--define-key map [rcscope-callers-tree]
      '(menu-item "Callers Tree"
        rcscope-callers-tree
        :help "Display tree of callers of function"))
    (bindings--define-key map [rcscope-assignments]
      '(menu-item "Find Assignments" rcscope-assignments
        :help "Find all assignments to specified symbol"))
    (bindings--define-key map [rcscope-includers]
      '(menu-item "Find Files Including" rcscope-includers
        :help "Find all files #including specified file"))
    (bindings--define-key map [rcscope-string]
      '(menu-item "Find String" rcscope-string
        :help "Find all occurrences of specified text string"))
    (bindings--define-key map [rcscope-callers]
      '(menu-item "Find Callers" rcscope-callers
        :help "Find all functions calling specified function"))
    (bindings--define-key map [rcscope-called-functions]
      '(menu-item "Find Called" rcscope-called-functions
        :help "Find all functions called by specified function"))
    (bindings--define-key map [rcscope-global-definition]
      '(menu-item "Find Definition" rcscope-global-definition
        :help "Find global definition of specified symbol"))
    (bindings--define-key map [rcscope-symbol]
      '(menu-item "Find Symbol" rcscope-symbol
        :help "Find occurrences of symbol"))
    map)
  "Menu keymap for issuing cscope queries.")

(defvar rcscope-results-mode-menu-map
  (let ((map (make-sparse-keymap)))
    (set-keymap-parent map rcscope-mode-menu-map)
    (bindings--define-key map [separator-1] menu-bar-separator)
    (bindings--define-key map [revert-buffer]
      '(menu-item "Re-run query" revert-buffer
        :help "Update cscope database and re-run last query"))
    (bindings--define-key map [separator-2] menu-bar-separator)
    (bindings--define-key map [next-error-follow-minor-mode]
      '(menu-item "Auto Display Match" next-error-follow-minor-mode
        :help "Display matching line automatically when moving the cursor"
        :button (:toggle . next-error-follow-minor-mode)))
    (bindings--define-key map [compilation-first-error]
      '(menu-item "First Match" first-error
        :help "Restart at the first match, visit corresponding location"))
    (bindings--define-key map [compilation-previous-error]
      '(menu-item "Previous Match" previous-error
        :help "Visit the previous match and corresponding location"))
    (bindings--define-key map [compilation-next-error]
      '(menu-item "Next Match" next-error
        :help "Visit the next match and corresponding location"))
    map)
  "Menu keymap for `rcscope-results-mode'.")

(defvar rcscope-prefix-map
  (let ((map (make-sparse-keymap)))
    (define-key map "s" 'rcscope-symbol)
    (define-key map "g" 'rcscope-global-definition)
    (define-key map "c" 'rcscope-called-functions)
    (define-key map "w" 'rcscope-callers)
    (define-key map "t" 'rcscope-string)
    (define-key map "i" 'rcscope-includers)
    (define-key map "a" 'rcscope-assignments)
    (define-key map "x" 'rcscope-callers-tree)
    map)
  "Prefix map for modes that can use Cscope.
Recommended place to bind this is M-s c (under `search-map')")

;; short name that displays nicer in which-key-mode
(fset 'rcscope-prefix rcscope-prefix-map)

(defvar rcscope-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map rcscope-keymap-prefix 'rcscope-prefix)
    (bindings--define-key map [menu-bar rcscope]
      (cons "Cscope" rcscope-mode-menu-map))
    map)
  "Keymap for `rcscope-mode'.")

(defvar rcscope-results-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "q"                  'quit-window)
    (define-key map (kbd "RET")          'rcscope-goto-match)
    (define-key map [mouse-2]            'rcscope-goto-match)
    (define-key map "\C-o"               'rcscope-display-match)
    (define-key map "\C-c\C-f"           'next-error-follow-minor-mode)
    (define-key map rcscope-keymap-prefix 'rcscope-prefix)
    (bindings--define-key map [menu-bar rcscope]
      (cons "Cscope" rcscope-results-mode-menu-map))
    map)
  "Keymap for `rcscope-results-mode'.")

(defvar rcscope-output-buffer-name "*Cscope*"
  "Default cscope results buffer name.")

(defvar rcscope-history () "History list for rcscope.")

(defvar rcscope--query-descriptions
  '(0 "Find symbol"
    1 "Find global definition"
    2 "Find functions called by this function"
    3 "Find functions calling this function"
    4 "Find this text string"
    8 "Find files #including this file"
    9 "Find assignments to this symbol"
    -1 "Display function's caller tree for"))

(defun rcscope--interactive (query-number default)
  (list
   (read-string
    (concat (plist-get rcscope--query-descriptions query-number)
            (when default
              (format " (default %s)"
                      default))
            ": ")
    nil
    'rcscope-history
    default)))

(defun rcscope-symbol (symbol)
  "Find SYMBOL in source code."
  (interactive (rcscope--interactive 0 (thing-at-point 'symbol)))
  (rcscope--handle-query 0 symbol))

(defun rcscope-global-definition (symbol)
  "Find SYMBOL's global definition."
  (interactive (rcscope--interactive 1 (thing-at-point 'symbol)))
  (rcscope--handle-query 1 symbol))

(defun rcscope-called-functions (function-name)
  "Find functions called by FUNCTION-NAME."
  (interactive (rcscope--interactive 2 (thing-at-point 'symbol)))
  (rcscope--handle-query 2 function-name))

(defun rcscope-callers (function-name)
  "Find functions calling FUNCTION-NAME."
  (interactive (rcscope--interactive 3 (thing-at-point 'symbol)))
  (rcscope--handle-query 3 function-name))

(defun rcscope-string (text-string)
  "Find occurences of TEXT-STRING."
  (interactive (rcscope--interactive
                4
                (or (save-mark-and-excursion
                     (er/mark-outside-quotes)
                     (when (use-region-p)
                       (buffer-substring (region-beginning)
                                         (region-end))))
                    (current-word))))
  (rcscope--handle-query 4 text-string))

(defun rcscope-includers (include-file)
  "Find all files #including INCLUDE-FILE."
  (interactive (rcscope--interactive 8 (thing-at-point 'filename)))
  (rcscope--handle-query 8 include-file))

(defun rcscope-assignments (symbol)
  "Find all assignments to SYMBOL."
  (interactive (rcscope--interactive 9 (thing-at-point 'symbol)))
  (rcscope--handle-query 9 symbol))

(defun rcscope-callers-tree (function-name depth)
  "Display tree of callers of FUNCTION-NAME to DEPTH in a format
similar to Steve Baker's Tree directory listing utility."
  (interactive
   (append (rcscope--interactive -1 (thing-at-point 'symbol))
           (list (read-number "Depth: " 4))))
  (rcscope--handle-query-callers-tree function-name depth))

;;; Result buffer navigation

(defun rcscope-goto-match (&optional event)
  "Open matched file in other window."
  (interactive (list last-input-event))
  (if event (posn-set-point (event-end event)))
  (let ((line-number (get-text-property (point) 'rcscope-line-number)))
    (when line-number
      (setq next-error-last-buffer (current-buffer))
      (let ((cscope-match-line (point-marker))
            (highlight-regexp (get-text-property
                               (point) 'rcscope-highlight-regexp))
            (highlight-length (get-text-property
                               (point) 'rcscope-highlight-length))
            match-start match-end)
        (with-current-buffer (find-file-noselect
                              (expand-file-name
                               (get-text-property (point)
                                                  'rcscope-file-name)
                               default-directory))
          (save-restriction
            (widen)
            (goto-char (point-min))
            (forward-line (1- line-number))

            (if highlight-regexp
                (progn
                  (re-search-forward highlight-regexp
                                     (line-end-position)
                                     nil)
                  (goto-char (or (match-beginning 2)
                                 (match-beginning 0)))
                  (setq match-start (point-marker))
                  (forward-char highlight-length)
                  (setq match-end (point-marker)))
              (setq match-start (copy-marker (progn
                                               (back-to-indentation)
                                               (point)))
                    match-end (copy-marker (line-end-position)))))

          (compilation-goto-locus cscope-match-line
                                  match-start match-end)
          (run-hooks 'next-error-hook))))))

(defun rcscope-display-match ()
  (interactive)
  (save-selected-window
    (rcscope-goto-match)))

(defun rcscope-next-error (&optional argp reset)
  "Move to Nth (default 1) next match in rcscope-results-mode buffer.
Compatibility function for \\[next-error] invocations."
  (interactive "p")
  (with-current-buffer
      (if (next-error-buffer-p (current-buffer))
          (current-buffer)
        (next-error-find-buffer
         nil nil (lambda ()
                   (eq major-mode 'rcscope-results-mode))))
    (when reset
      (goto-char (point-min)))

    (let ((direction (if (< 0 argp) 1 -1))
          (i (abs argp)))
      (while (> i 0)
        (if (= 0 (forward-line direction))
            (when (get-text-property (point) 'rcscope-line-number)
              (decf i))
          (message "No more matches")
          (setq i 0))))

    ;; In case the *cscope* buffer is visible in a nonselected window.
    (let ((win (get-buffer-window (current-buffer) t)))
      (when win
        (set-window-point win (point))))

    (rcscope-goto-match)))

(defun rcscope--cleanup-procbuf ()
  (kill-buffer rcscope--cscope-process-buffer))

(put 'rcscope-results-mode 'mode-class 'special)
(define-derived-mode rcscope-results-mode special-mode "rcscope"
  "Major mode for cscope output.
\\<rcscope-results-mode-map>Move point to one of the cscope lookup
results in this buffer, then use \\[rcscope-goto-match] to visit
the lookup result file at the matching line.

\\{rcscope-results-mode-map}"
  (setq buffer-undo-list t)
  (setq next-error-function 'rcscope-next-error)
  (set (make-local-variable 'revert-buffer-function)
       'rcscope-revert-function)
  (add-hook 'kill-buffer-hook 'rcscope--cleanup-procbuf nil t))

(defvar-local rcscope--revert-command nil)
(defun rcscope-revert-function (_ignore1 _ignore2)
  "Handle `revert-buffer' for `rcscope-results-mode' buffers."
  (rcscope--kill-cscope-process)
  (eval rcscope--revert-command))

(defun rcscope--find-cscope-database-directory ()
  (or
    (let ((result nil)
          old-dir
          (dir (expand-file-name default-directory)))
      (while (and (not result) dir (not (string= old-dir dir)))
        (setq old-dir dir)
        (if (file-readable-p (expand-file-name "cscope.out" dir))
            (setq result dir)
          (setq dir (file-name-directory (directory-file-name dir)))))
      result)
    (condition-case nil
        (projectile-project-root)
      (error default-directory))))

(defvar rcscope--cscope-process-buffer nil)

(defun rcscope--kill-cscope-process ()
  (ignore-errors
    (delete-process
     (get-buffer-process rcscope--cscope-process-buffer))))

(defun rcscope--open-cscope-database (cscope-db-dir)
  (rcscope--kill-cscope-process)
  ;; leading space in the name turns off undo and hides buffer
  (setq rcscope--cscope-process-buffer
          (get-buffer-create " *cscope-process*"))
  (with-current-buffer rcscope--cscope-process-buffer
    (setq default-directory cscope-db-dir)
    (erase-buffer)
    (let ((cscope-process
            (apply #'start-file-process
                   "*cscope-process*" rcscope--cscope-process-buffer
                   "cscope" "-l" rcscope-cscope-options)))
      (set-process-filter cscope-process 'rcscope--process-filter)
      (set-process-query-on-exit-flag cscope-process nil)
      (accept-process-output cscope-process 3)
      (when (looking-at ".*cannot open.*cscope\.out.*")
        (error "cscope: %s" (buffer-string)))))
  rcscope--cscope-process-buffer)

(defun rcscope--cscope-process-buffer ()
  (if (and rcscope--cscope-process-buffer
           (get-buffer-process rcscope--cscope-process-buffer)
           (let ((process-dir (buffer-local-value
                               'default-directory
                               rcscope--cscope-process-buffer)))
             (and
               (string-prefix-p process-dir default-directory)
               (setq default-directory process-dir))))
      rcscope--cscope-process-buffer
    (rcscope--open-cscope-database
     (setq default-directory
             (rcscope--find-cscope-database-directory)))))

(defun rcscope--handle-query (type term)
  (let ((result-buf (rcscope--result-buffer type term)))
    (with-current-buffer result-buf
      (let ((procbuf (rcscope--cscope-process-buffer)))
        (setq rcscope--revert-command (list 'rcscope--handle-query
                                            type
                                            term))
        (display-buffer result-buf '(nil (allow-no-window . t)))
        (goto-char (point-max)) ; display-buffer seems to mess w/point
        (rcscope--cscope-exec-query procbuf
                                    (format "%d%s\n" type term))
        (rcscope--cscope-parse-output procbuf
                                      result-buf
                                      term
                                      type
                                      #'rcscope--results-group-by-file)
        (rcscope--finish-result-buffer
         (buffer-local-value 'rcscope--query-number-results procbuf)
         result-buf)))))

(defvar rcscope--newline (propertize "\n" 'read-only t))

;;; call trees

(defvar rcscope--ascii-only
  (not (every #'char-displayable-p "┌└├│─")))

(defvar-local rcscope--call-tree-level nil)
(defvar-local rcscope--tree-child-number-results nil)
(defvar-local rcscope--tree-omit-levels ())

(defun rcscope--handle-query-callers-tree (function-name depth)
  (let ((result-buf (rcscope--result-buffer -1 function-name)))
    (with-current-buffer result-buf
      (setq rcscope--revert-command
              (list 'rcscope--handle-query-callers-tree
                    function-name
                    depth))
      (let ((inhibit-read-only t)
            (procbuf (rcscope--cscope-process-buffer)))
        (insert (propertize
                 (concat
                  (if rcscope--ascii-only ". " "┌ ")
                  (propertize function-name
                              'face font-lock-function-name-face)
                  "\n")
                 'rcscope-function-name function-name))

        (dotimes (level depth)
          (setq rcscope--call-tree-level (+ 1 level))
          (goto-char (point-min))
          (while (re-search-forward (if (= level 0)
                                        (if rcscope--ascii-only
                                            "^. "
                                          "^┌ ")
                                      (format (if rcscope--ascii-only
                                                  "^[`|- ]\\{%d\\}"
                                                "^[└├│─ ]\\{%d\\}")
                                              (* 4 level)))
                                    nil t)
            (forward-line 0)
            (let ((caller-name (get-text-property
                                (point)
                                'rcscope-function-name))
                  (rcscope--tree-omit-levels (get-text-property
                                              (point)
                                              'rcscope-omit-levels)))
              (forward-line +1)
              (rcscope--cscope-exec-query
               procbuf (concat "3" caller-name "\n"))
              (setq rcscope--tree-child-number-results
                      (buffer-local-value
                       'rcscope--query-number-results procbuf))
              (rcscope--cscope-parse-output
               procbuf
               result-buf
               caller-name
               -1
               #'rcscope--results-group-by-callee))))))

    (rcscope--finish-result-buffer nil result-buf)
    (display-buffer result-buf '(nil (allow-no-window . t)))))

(defun rcscope--results-insert-caller (function-name
                                       level line-number
                                       matching-line file-name)
  (insert
   (propertize
    (concat (let ((p ""))
              (dotimes (i (- level 1))
                (setq p (concat
                         p
                         (if (member i rcscope--tree-omit-levels)
                             "    "
                           (if rcscope--ascii-only "|   " "│   ")))))
              p)
            (if (= 0 (decf rcscope--tree-child-number-results))
                (if rcscope--ascii-only "`-- " "└── ")
              (if rcscope--ascii-only "|-- " "├── "))
            (propertize function-name
                        'face font-lock-function-name-face)
            " "
            (propertize
             (concat
              (if file-name
                  (propertize file-name
                              'face 'compilation-info)
                "")
              (propertize (format ":%d" line-number)
                          'face list-matching-lines-prefix-face))
             'mouse-face '(highlight)
             'follow-link t
             'help-echo "mouse-2: visit the call location")

            "\n")
    'rcscope-function-name function-name
    'rcscope-line-number   line-number
    'rcscope-file-name     file-name
    'rcscope-omit-levels   (if (= 0 rcscope--tree-child-number-results)
                               (cons (- level 1)
                                     rcscope--tree-omit-levels)
                             rcscope--tree-omit-levels))))

(defun rcscope--results-group-by-callee (term highlight-regexp
                                         highlight-length
                                         buf file line-number
                                         function-name matching-line)
  (with-current-buffer buf
    (rcscope--results-insert-caller function-name
                                    rcscope--call-tree-level
                                    line-number
                                    matching-line
                                    file)))

;;; cscope process management

(defun rcscope--process-filter (process string)
  (with-current-buffer (process-buffer process)
    (save-excursion
      (goto-char (point-max))
      (insert string))))

(defvar-local rcscope--query-number-results nil)

(defun rcscope--wait-for-output (procbuf)
  (with-timeout
      (5
       (pop-to-buffer procbuf)
       (error "cscope process is taking more than 5 seconds to respond"))
    (let ((proc (get-buffer-process procbuf))
          (start-point (point)))
      (save-excursion
        (while (not (looking-at "^>>"))
          (accept-process-output proc 1)
          (goto-char (point-max))
          (forward-line 0)))

      ;; Find the number of results returned by the search
      (setq rcscope--query-number-results 0)
      (goto-char start-point)
      (when (re-search-forward "^cscope: \\([0-9]+\\) lines$" nil t)
        (setq rcscope--query-number-results
                (string-to-number (match-string 1)))
        (forward-line 1))
      rcscope--query-number-results)))

(defun rcscope--cscope-exec-query (procbuf command)
  "Returns t if the query returned 1 or more results, nil otherwise."
  (with-current-buffer procbuf
    (goto-char (point-max))
    (insert "\n" command)
    (process-send-string (get-buffer-process procbuf) command)
    (> (rcscope--wait-for-output procbuf) 0)))

;;; cscope output parsing and pretty-printing

(defun rcscope--cscope-parse-output (procbuf resultbuf term type
                                             grouping-func)
  (let (line
        (stall 0)
        (cscope-regexp
          "\\([^[:blank:]]*\\)[[:blank:]]+\\([^[:blank:]]*\\)[[:blank:]]+\\([[:digit:]]*\\)[[:blank:]]+\\(.*\\)"))
    (with-current-buffer procbuf
      (while (and (< (point) (point-max))
                  (= 0 stall))
	(setq line (buffer-substring-no-properties
                    (line-beginning-position)
                    (line-end-position)))
	(setq stall (forward-line 1))

	(when (string-match cscope-regexp line)
          (let ((function-name (match-string 2 line)))
            (funcall
             grouping-func
             term
             (if (= type 4)
                 (regexp-quote term)
               (format "\\([^a-zA-Z_]\\|^\\)\\(%s\\)\\([^a-zA-Z0-9]\\|$\\)"
                       (regexp-quote (if (= type 2)
                                         function-name
                                       term))))
             (length (if (= type 2) function-name term))
             resultbuf
             (match-string 1 line) ; file
             (string-to-number (match-string 3 line)) ; lnum
             function-name
             (match-string 4 line)))))))) ; matching line

(defvar-local rcscope--last-file-context nil)

(defun rcscope--results-group-by-file (term highlight-regexp
                                       highlight-length
                                       buf filename line-number
                                       function-name matching-line)
  (with-current-buffer buf
    (let ((inhibit-read-only t))
      (unless (equal filename rcscope--last-file-context)
        (setq rcscope--last-file-context filename)
        (insert (propertize filename
                            'face      'compilation-info
                            'read-only t)
                rcscope--newline))

      ;; Insert the found result
      ;; colorize the term in the matching line if possible
      (let ((term-match-start (string-match highlight-regexp
                                            matching-line)))
        (when term-match-start
          (let ((start-pos (or (match-beginning 2) term-match-start)))
            (add-face-text-property
             start-pos (+ start-pos highlight-length)
             list-matching-lines-face nil matching-line))))
      ;; replace.el claims "Using 7 digits aligns tabs properly"
      (insert (propertize
               (concat
                (propertize
                 (concat
                  (propertize (format "%7d:" line-number)
                              'face list-matching-lines-prefix-face
                              'read-only t)
                  (propertize function-name
                              'face font-lock-function-name-face
                              'read-only t)
                  (propertize ":"
                              'face list-matching-lines-prefix-face
                              'read-only t)
                  matching-line)
                 'mouse-face '(highlight)
                 'follow-link t
                 'help-echo "mouse-2: visit the source location")
                rcscope--newline)
               'rcscope-file-name        filename
               'rcscope-line-number      line-number
               'rcscope-highlight-regexp highlight-regexp
               'rcscope-highlight-length highlight-length)))))

(defun rcscope--result-buffer (query-number term)
  (let ((result-buf (if (eq major-mode 'rcscope-results-mode)
                        (current-buffer)
                      (get-buffer-create rcscope-output-buffer-name)))
        (cur-dir default-directory))
    (with-current-buffer result-buf
      (setq default-directory cur-dir)
      (let ((inhibit-read-only t))
        (erase-buffer)
        (rcscope-results-mode)
        (insert (propertize
                 (concat (plist-get rcscope--query-descriptions
                                    query-number)
                         ": "
                         term)
                 'face      list-matching-lines-buffer-name-face
                 'read-only t)
                rcscope--newline)))
    result-buf))

(defun rcscope--finish-result-buffer (number-results result-buf)
  (with-current-buffer result-buf
    (let ((results-end       (goto-char (point-max)))
          (inhibit-read-only t))
      (insert "\ncscope finished")
      (when number-results
        (insert " with "
                (format "%d" number-results)
                (if (= number-results 1) " match" " matches")))
      (insert " at " (substring (current-time-string) 0 19))
      (put-text-property results-end (point) 'read-only t))
    (goto-char (point-min))
    (setq next-error-last-buffer result-buf)))

;;;###autoload
(define-minor-mode rcscope-mode "Minor mode for issuing Cscope queries"
  :keymap rcscope-mode-map)

(provide 'rcscope)

;;; rcscope.el ends here
